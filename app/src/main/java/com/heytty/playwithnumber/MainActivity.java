package com.heytty.playwithnumber;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    EditText editText;
    int randomNumber;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.input);
        textView = findViewById(R.id.viewResult);
        Random random = new Random();
        randomNumber = random.nextInt(20) + 1;
    }


    public void guessNumber(View view) {
        int guessInt = Integer.parseInt(editText.getText().toString());
        if (guessInt < randomNumber) {
            String value = textView.getText().toString();
            textView.setText("Please enter higher number.");
        } else if (guessInt > randomNumber) {
            String value = textView.getText().toString();
            textView.setText("Please enter lower number.");
        } else {
            String value = textView.getText().toString();
            textView.setText("Congrats! You entered correct number.");
        }
    }
}
